model CarSharingSystemCaseStudy
  // Parameters
  parameter Integer numCars_1 = 10 "Number of cars in the system at starting point";
  parameter Integer numUsers_1 = 5 "Number of users in the system at starting point";
  parameter Integer numCars_2 = 9 "Number of cars in the system at starting point";
  parameter Integer numUsers_2 = 5 "Number of users in the system at starting point";
  parameter Real userArrivalRate = 1.0 "User arrival rate (users per time unit)";
  parameter Real returnRate = 0.5 "Car return rate";
  parameter Real chargingTime = 1.0 "Time to fully charge a car";
  parameter Real usageTime = 1.0 "Car usage time by a customer";
  parameter Real userPatience = 10.0 "Maximum waiting time for users";
  // State variables for objectives
  Real totalWaitingTime(start = 0) "Total waiting time for users";
  Real totalCarsRented(start = 0) "Total number of cars rented";
  Real totalChargingTime(start = 0) "Total charging time for all cars";
  discrete Integer usersLeaving(start = 0);
  // Discrete Place: Number of free places in the station
  PNlib.Components.PD PSCii(nIn = 1, nOut = 1) annotation(
    Placement(transformation(origin = {-330, 130}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  // Stochastic Transition: Car is ready and available for users after charging operation
  PNlib.Components.TES TCRi(nIn = 1, nOut = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/chargingTime) annotation(
    Placement(transformation(origin = {-282, 56}, extent = {{-10, -10}, {10, 10}})));
  // Discrete Place: Cars parked at the station, ready and then available for users
  PNlib.Components.PD PSRi(nIn = 1, nOut = 1, startTokens = numCars_1) annotation(
    Placement(transformation(origin = {-244, 122}, extent = {{-10, -10}, {10, 10}})));
  // Discrete Place: Number of free places in the station
  PNlib.Components.PD PSCi(nIn = 1, nOut = 1, startTokens = 0) annotation(
    Placement(transformation(origin = {-242, 156}, extent = {{10, -10}, {-10, 10}})));
  // Immediate Transition: User demand "satisfied" and then "user departure" from station
  PNlib.Components.T TUSi(nIn = 2, nOut = 2) annotation(
    Placement(transformation(origin = {-182, 122}, extent = {{-14, -14}, {14, 14}})));
  // Discrete Place: Car in use by customers
  PNlib.Components.PD PCU(nIn = 3, nOut = 4, startTokens = 5) annotation(
    Placement(transformation(origin = {1, 17}, extent = {{21, -21}, {-21, 21}}, rotation = -90)));
  // Discrete Place: Users waiting during a given random delay- for available and ready cars in the station
  PNlib.Components.PD PSDi(nIn = 1, nOut = 2, startTokens = numUsers_1) annotation(
    Placement(transformation(origin = {-304, -12}, extent = {{-10, -10}, {10, 10}})));
  // Deterministic transition: User demand “not satisfied” at station
  PNlib.Components.TD TUNi(nIn = 1, delay = userPatience) annotation(
    Placement(transformation(origin = {-214, -12}, extent = {{-10, -10}, {10, 10}})));
  // Stochastic Transition: User demand arrival at station
  PNlib.Components.TES TUDi(nOut = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/userArrivalRate) annotation(
    Placement(transformation(origin = {-348, -12}, extent = {{-10, -10}, {10, 10}})));
  // Stochastic Transition: Car return to station by user. Then, the car is in a charging situation
  PNlib.Components.TES TCCi(nIn = 2, nOut = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/chargingTime) annotation(
    Placement(transformation(origin = {-318, 166}, extent = {{10, -10}, {-10, 10}})));
  // Stochastic Transition: Car picked up for “long” maintenance (e.g. due to an incident/ accident) by service provider
  PNlib.Components.TES TCM(nIn = 1, nOut = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/chargingTime) annotation(
    Placement(transformation(origin = {222, 160}, extent = {{-10, -10}, {10, 10}})));
  // Discrete Place: Cars under maintenance
  PNlib.Components.PD PCM(nIn = 1, nOut = 1, startTokens = 2) annotation(
    Placement(transformation(origin = {384, 160}, extent = {{-10, -10}, {10, 10}})));
  // Stochastic Transition: After maintenance, car is parked at the station Park. It is ready and available for users
  PNlib.Components.TES TMPR(nIn = 1, nOut = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/usageTime) annotation(
    Placement(transformation(origin = {468, 74}, extent = {{-10, -10}, {10, 10}})));
  // Discrete Place: Car in use by customers
  PNlib.Components.PD PCU_1(nIn = 2, nOut = 1, startTokens = 3) annotation(
    Placement(transformation(origin = {392, -8}, extent = {{10, -10}, {-10, 10}})));
  // Stochastic Transition: The car is ready and available for other users, after its charging operation
  PNlib.Components.TES TCPR(nIn = 1, nOut = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/returnRate) annotation(
    Placement(transformation(origin = {342, -94}, extent = {{-10, -10}, {10, 10}})));
  // Discrete Place: Car in use by customers
  PNlib.Components.PD PCU_2(nIn = 1, nOut = 1, startTokens = 1) annotation(
    Placement(transformation(origin = {296, -130}, extent = {{-10, -10}, {10, 10}})));
  // Stochastic transition: Car return to station Park by user. This car will be in charging operation
  PNlib.Components.TES TCPc(nIn = 1, nOut = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/usageTime) annotation(
    Placement(transformation(origin = {186, -92}, extent = {{-10, -10}, {10, 10}})));
  // Immediate Transition: User demand “satisfied” and then “user departure” from station park
  PNlib.Components.T TUSp(nIn = 2, nOut = 1) annotation(
    Placement(transformation(origin = {113, -7}, extent = {{21, -21}, {-21, 21}})));
  // Stochastic Transition: User demand arrival at station park
  PNlib.Components.TES TUDp(nIn = 0, nOut = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/userPatience) annotation(
    Placement(transformation(origin = {336, 74}, extent = {{10, -10}, {-10, 10}})));
  // Helper transition
  PNlib.Components.PD P1(nIn = 1, nOut = 2) annotation(
    Placement(transformation(origin = {280, 74}, extent = {{10, -10}, {-10, 10}})));
  // Stochastic Transition: User demand “not satisfied” at Park. The user leaves the station without having been served
  PNlib.Components.TDS TUNp(nIn = 1, distributionType = PNlib.Types.DistributionType.Exponential, h = 1/userPatience) annotation(
    Placement(transformation(origin = {144, 74}, extent = {{10, -10}, {-10, 10}})));
  inner PNlib.Components.Settings settings(showTokenFlow = true) annotation(
    Placement(transformation(origin = {-470, 188}, extent = {{-22, -22}, {22, 22}})));
  PNlib.Components.PD PSCii1(nIn = 1, nOut = 1) annotation(
    Placement(transformation(origin = {-362, -196}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PNlib.Components.TES TCRi1(distributionType = PNlib.Types.DistributionType.Exponential, h = 1/chargingTime, nIn = 1, nOut = 1) annotation(
    Placement(transformation(origin = {-314, -270}, extent = {{-10, -10}, {10, 10}})));
  PNlib.Components.PD PSRi1(nIn = 1, nOut = 1, startTokens = numCars_2) annotation(
    Placement(transformation(origin = {-276, -204}, extent = {{-10, -10}, {10, 10}})));
  PNlib.Components.PD PSCi1(nIn = 1, nOut = 1, startTokens = 0) annotation(
    Placement(transformation(origin = {-274, -170}, extent = {{10, -10}, {-10, 10}})));
  PNlib.Components.T TUSi1(nIn = 2, nOut = 2) annotation(
    Placement(transformation(origin = {-214, -204}, extent = {{-14, -14}, {14, 14}})));
  PNlib.Components.PD PSDi1(nIn = 1, nOut = 2, startTokens = numUsers_2) annotation(
    Placement(transformation(origin = {-298, -378}, extent = {{-10, -10}, {10, 10}})));
  PNlib.Components.TD TUNi1(delay = userPatience, nIn = 1) annotation(
    Placement(transformation(origin = {-208, -378}, extent = {{-10, -10}, {10, 10}})));
  PNlib.Components.TES TUDi1(distributionType = PNlib.Types.DistributionType.Exponential, h = 1/userArrivalRate, nOut = 1) annotation(
    Placement(transformation(origin = {-342, -378}, extent = {{-10, -10}, {10, 10}})));
  PNlib.Components.TES TCCi1(distributionType = PNlib.Types.DistributionType.Exponential, h = 1/chargingTime, nIn = 2, nOut = 1) annotation(
    Placement(transformation(origin = {-350, -160}, extent = {{10, -10}, {-10, 10}})));
equation
// Calculate total waiting time when users leave
  when change(PSDi.tokenFlow.outflow[2]) then
    usersLeaving = PSDi.tokenFlow.outflow[2];
// Number of users leaving
    totalWaitingTime = pre(totalWaitingTime) + usersLeaving*userPatience;
  end when;
// Calculate total number of cars rented
  when change(PSRi.tokenFlow.outflow[1]) then
    totalCarsRented = PSRi.tokenFlow.outflow[1];
  end when;
// Calculate total charging time
  when TCCi.active then
    totalChargingTime = pre(totalChargingTime) + chargingTime;
  end when;
// Connections
  connect(TUDi.outPlaces, PSDi.inTransition) annotation(
    Line(points = {{-343.2, -12}, {-313.2, -12}}, thickness = 0.5));
  connect(PSRi.outTransition[1], TUSi.inPlaces[1]) annotation(
    Line(points = {{-233.2, 122}, {-189, 122}}, thickness = 0.5));
  connect(PSDi.outTransition[1], TUSi.inPlaces[2]) annotation(
    Line(points = {{-293.2, -12}, {-215.4, -12}, {-215.4, 122}, {-189.2, 122}}, thickness = 0.5));
  connect(PSDi.outTransition[2], TUNi.inPlaces[1]) annotation(
    Line(points = {{-293.2, -12}, {-217.2, -12}}, thickness = 0.5));
  connect(PSCi.outTransition[1], TCCi.inPlaces[1]) annotation(
    Line(points = {{-252.8, 156}, {-269, 156}, {-269, 166}, {-313, 166}}, thickness = 0.5));
  connect(TCCi.outPlaces[1], PSCii.inTransition[1]) annotation(
    Line(points = {{-322.8, 166}, {-330.6, 166}, {-330.6, 141}, {-329.8, 141}}, thickness = 0.5));
  connect(PSCii.outTransition[1], TCRi.inPlaces[1]) annotation(
    Line(points = {{-330, 119.2}, {-330, 56.2}, {-287, 56.2}}, thickness = 0.5));
  connect(TCRi.outPlaces[1], PSRi.inTransition[1]) annotation(
    Line(points = {{-277.2, 56}, {-277.2, 122}, {-253.4, 122}}, thickness = 0.5));
  connect(TCM.outPlaces[1], PCM.inTransition[1]) annotation(
    Line(points = {{227, 160}, {373, 160}}, thickness = 0.5));
  connect(PCM.outTransition[1], TMPR.inPlaces[1]) annotation(
    Line(points = {{395, 160}, {449, 160}, {449, 73.5215}, {463, 73.5215}, {463, 74}}, thickness = 0.5));
  connect(TMPR.outPlaces[1], PCU_1.inTransition[1]) annotation(
    Line(points = {{473, 74}, {450, 74}, {450, -8}, {403, -8}}, thickness = 0.5));
  connect(TCPR.outPlaces[1], PCU_1.inTransition[2]) annotation(
    Line(points = {{347, -94}, {347, -51}, {403, -51}, {403, -8}}, thickness = 0.5));
  connect(PCU_2.outTransition[1], TCPR.inPlaces[1]) annotation(
    Line(points = {{307, -130}, {323, -130}, {323, -94}, {337, -94}}, thickness = 0.5));
  connect(TCPc.outPlaces[1], PCU_2.inTransition[1]) annotation(
    Line(points = {{191, -92}, {241, -92}, {241, -130}, {285, -130}}, thickness = 0.5));
  connect(PCU_1.outTransition[1], TUSp.inPlaces[1]) annotation(
    Line(points = {{381, -8}, {123, -8}, {123, -7}}, thickness = 0.5));
  connect(TUDp.outPlaces[1], P1.inTransition[1]) annotation(
    Line(points = {{331, 74}, {291, 74}}, thickness = 0.5));
  connect(P1.outTransition[1], TUNp.inPlaces[1]) annotation(
    Line(points = {{269, 74}, {149, 74}}, thickness = 0.5));
  connect(P1.outTransition[2], TUSp.inPlaces[2]) annotation(
    Line(points = {{269, 74}, {266, 74}, {266, 22}, {123, 22}, {123, -7}}, thickness = 0.5));
  connect(PCU.outTransition[1], TCCi.inPlaces[2]) annotation(
    Line(points = {{1, 40}, {2.25, 40}, {2.25, 188}, {2, 188}, {2, 189}, {-103, 189}, {-103, 189.25}, {-261, 189.25}, {-261, 189.312}, {-301, 189.312}, {-301, 188.656}, {-313, 188.656}, {-313, 166}}, thickness = 0.5));
  connect(PCU.outTransition[2], TCM.inPlaces[1]) annotation(
    Line(points = {{1, 40}, {108, 40}, {108, 95}, {199, 95}, {199, 160.812}, {217, 160.812}, {217, 160}}, thickness = 0.5));
  connect(PCU.outTransition[3], TCPc.inPlaces[1]) annotation(
    Line(points = {{1, 40}, {163, 40}, {163, -92}, {181, -92}}, thickness = 0.5));
  connect(TUSi.outPlaces[1], PCU.inTransition[1]) annotation(
    Line(points = {{-175, 122}, {-100, 122}, {-100, -48}, {0.5, -48}, {0.5, -6}, {1, -6}}, thickness = 0.5));
  connect(TUSi.outPlaces[2], PSCi.inTransition[1]) annotation(
    Line(points = {{-175.28, 122}, {-159.28, 122}, {-159.28, 156}, {-232.28, 156}}, thickness = 0.5));
  connect(TUSp.outPlaces[1], PCU.inTransition[2]) annotation(
    Line(points = {{103, -7}, {53.5, -7}, {53.5, -6}, {1, -6}}, thickness = 0.5));
  connect(TUDi1.outPlaces, PSDi1.inTransition) annotation(
    Line(points = {{-337.2, -378}, {-307.2, -378}}, thickness = 0.5));
  connect(PSRi1.outTransition[1], TUSi1.inPlaces[1]) annotation(
    Line(points = {{-265.2, -204}, {-221, -204}}, thickness = 0.5));
  connect(PSDi1.outTransition[1], TUSi1.inPlaces[2]) annotation(
    Line(points = {{-287.2, -378}, {-159.6, -378}, {-159.6, -204}, {-221.2, -204}}, thickness = 0.5));
  connect(PSDi1.outTransition[2], TUNi1.inPlaces[1]) annotation(
    Line(points = {{-287.2, -378}, {-211.2, -378}}, thickness = 0.5));
  connect(PSCi1.outTransition[1], TCCi1.inPlaces[1]) annotation(
    Line(points = {{-284.8, -170}, {-301, -170}, {-301, -160}, {-345, -160}}, thickness = 0.5));
  connect(TCCi1.outPlaces[1], PSCii1.inTransition[1]) annotation(
    Line(points = {{-354.8, -160}, {-362.6, -160}, {-362.6, -185}, {-361.8, -185}}, thickness = 0.5));
  connect(PSCii1.outTransition[1], TCRi1.inPlaces[1]) annotation(
    Line(points = {{-362, -206.8}, {-362, -269.8}, {-319, -269.8}}, thickness = 0.5));
  connect(TCRi1.outPlaces[1], PSRi1.inTransition[1]) annotation(
    Line(points = {{-309.2, -270}, {-309.2, -204}, {-285.4, -204}}, thickness = 0.5));
  connect(TUSi1.outPlaces[1], PSCi1.inTransition[1]) annotation(
    Line(points = {{-207.28, -204}, {-191.28, -204}, {-191.28, -170}, {-264.28, -170}}, thickness = 0.5));
  connect(TUSi1.outPlaces[2], PCU.inTransition[3]) annotation(
    Line(points = {{-208, -204}, {2, -204}, {2, -6}}, thickness = 0.5));
  connect(PCU.outTransition[4], TCCi1.inPlaces[2]) annotation(
    Line(points = {{2, 40}, {-84.5, 40}, {-84.5, -138}, {-346, -138}, {-346, -160}}, thickness = 0.5));
  annotation(
    uses(PNlib(version = "3.0.0"), Modelica(version = "4.0.0")),
    Diagram(graphics = {Text(origin = {-371, -36}, extent = {{3, 0}, {-3, 0}}, textString = "text"), Text(origin = {-312, 234}, extent = {{42, -20}, {-42, 0}}, textString = "Station subnet", fontSize = 12, fontName = "Open Sans"), Rectangle(origin = {-263, 118}, lineColor = {0, 0, 255}, lineThickness = 0.5, extent = {{-121, 98}, {121, -98}}), Rectangle(origin = {-280, -12}, lineColor = {0, 255, 0}, lineThickness = 0.5, extent = {{-104, 30}, {104, -30}}), Text(origin = {-329, -54}, extent = {{55, -6}, {-55, 0}}, textString = "User demand subnet", fontSize = 12, fontName = "Open Sans"), Rectangle(origin = {227, 31}, lineColor = {0, 170, 255}, lineThickness = 0.5, extent = {{-135, 71}, {135, -71}}), Text(origin = {113, 140}, extent = {{85, -54}, {-85, 0}}, textString = "User Demand (park) subnet", fontSize = 12, fontName = "Open Sans"), Polygon(origin = {342, 55}, lineColor = {255, 85, 255}, lineThickness = 0.5, points = {{-154, 111}, {-154, 57}, {32, 57}, {32, -139}, {142, -139}, {142, 137}, {-154, 137}, {-154, 127}, {-154, 111}}), Text(origin = {273, 204}, extent = {{-117, 26}, {117, -26}}, textString = "Car Maintenance (Center) subnet", fontSize = 12, fontName = "Open Sans"), Rectangle(origin = {293, -77}, lineColor = {85, 85, 0}, lineThickness = 0.5, extent = {{-145, 87}, {145, -87}}), Text(origin = {232, -176}, extent = {{-92, 20}, {92, -20}}, textString = "Car-Sharing Park (Center) subnet", fontSize = 12, fontName = "Open Sans"), Text(extent = {{-16, 40}, {-16, 40}}, textString = "text"), Text(origin = {-365, -402}, extent = {{3, 0}, {-3, 0}}, textString = "text"), Text(origin = {-344, -92}, extent = {{42, -20}, {-42, 0}}, textString = "Station subnet", fontSize = 12, fontName = "Open Sans"), Rectangle(origin = {-261, -224}, lineColor = {0, 0, 255}, lineThickness = 0.5, extent = {{-121, 112}, {121, -112}}), Rectangle(origin = {-274, -378}, lineColor = {0, 255, 0}, lineThickness = 0.5, extent = {{-104, 30}, {104, -30}}), Text(origin = {-323, -420}, extent = {{55, -6}, {-55, 0}}, textString = "User demand subnet", fontSize = 12, fontName = "Open Sans")}, coordinateSystem(extent = {{-500, 240}, {500, -440}})),
    version = "");
end CarSharingSystemCaseStudy;

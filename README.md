# PN4ECSS

Petri Net for Electric Car-Sharing System (ECSS).

## Introduction

Implementation and modification of the ECSS Petri Net model in Modelica based on [this paper](https://www.e3s-conferences.org/articles/e3sconf/abs/2020/30/e3sconf_evf2020_03001/e3sconf_evf2020_03001.html).

Three **objective functions** have been implemented:

* `totalWaitingTime`: Calculate the total waiting time of users

* `totalChargingTime`: Calculate the total charging time of cars

* `CarsRented`: Calculate the rented cars in total

In an optimization problem, `totalWaitingTime` and `totalChargingTime` need to be minimized, `CarsRented` needs to be maximized.

## Demo

Modeling Electric Car-sharing Systems with Petri nets using Modelica and Optimization using MOO4Modelica

https://youtu.be/LOc7q_BexsY

## Environments

[OpenModelica v1.22.3](https://openmodelica.org/download/download-windows/)

[PNlib v3.0.0](https://github.com/AMIT-HSBI/PNlib)

## Model Description

![](./assets/CarSharingSystemCaseStudy.png)

#### 1. Station Subnet

* TCCi: Car return to the station and then in a charging situation.
* PSCi: Number of free places in the station.
* TCRi: Car is ready and available for users after charging operation.
* PSRi: Cars parked at the station, ready and then available for users.
* TUSi: User demand "satisfied" and then "user departure" from station.
* PCU: Car in use.

#### 2. User Demand Subnet

* TUDi: User demand arrival at station.
* PSDi: Users waiting during a given random delay- for available and ready cars in the station.
* TUNi: User demand “not satisfied”, user leaves.

#### 3. Car-sharing Park (Center) Subnet

* TCPc: Car return to station Park by user. This car will be in charging operation.
* TCPR: The car is ready and available for other users, after its charging operation.

#### 4. User Demand (Park) Subnet

* TUNp: User demand “not satisfied” at park. User leaves.

* TUDp: User demand arrival at station park.
* TUSp: User demand “satisfied” and then “user departure” from station park

#### 5. Car Maintenance (Center) Subnet

* TCM: Car picked up for “long” maintenance (e.g. due to an incident/ accident) by service provider
* PCM: Cars under maintenance
* TMPR: After maintenance, car is parked at the station Park. It is ready and available for users

## How Tokens are Generated and Flow Through the Model

#### 1. User Arrival and Demand (TUDi and TUDp)

* Tokens representing user demand are generated at the `TUDi` and `TUDp` transitions, which simulate user arrivals at the station and park, respectively. The rate of token generation is governed by an exponential distribution based on `userPatience`.

#### 2. Users Waiting (PSDi)

* Arriving users are represented as tokens in the `PSDi` place, where they wait for available cars. If cars are available, tokens (users) will move to the `TUSi` transition where they pick up a car.

#### 3. Car Pickup and Usage (TUSi and PCU)

* Tokens (users) that successfully pick up cars move to the `PCU` place, representing cars in use by customers. This transition reduces the number of tokens in the `PSRi` place (cars ready) and increases the tokens in `PCU` (cars in use).

#### 4. Car Return and Charging (TCCi, TCRi, and PSCii)

* After usage, cars are returned and go through a charging process. The `TCCi` transition moves tokens from `PCU` to `PSCii` (charging state). Once charging is complete, tokens move through `TCRi` back to `PSRi` (cars ready).

#### 5. Car Maintenance (TCM, TMPR, and PCM)

* Occasionally, cars require maintenance. The `TCM` transition moves tokens to `PCM` (cars under maintenance). After maintenance, tokens move back to the `TMPR` transition, eventually returning to the pool of available cars.

#### 6. User Departure (TUNi and TUNp)

* If a user waits too long for a car, they leave the system. This is represented by the `TUNi` and `TUNp` transitions, which move tokens from the `PSDi` place without satisfying their demand.
